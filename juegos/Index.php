<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Inicio</title>
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <div>
        <form action="buscar.php" method="post">
            <input type="text" name="buscar" id="">
            <input type="submit" value="Buscar">
            <a class="enlace" href="nuevo.php">Agregar Nuevo Juego</a>
        </form>
    </div>
    <div>
        <table border="1">
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Descripción</td>
                <td>Categoria</td>
                <td>Valor</td>
                <td>Acciones</td>
            </tr>
            <?php
            $conn = mysqli_connect("localhost", "root", "", "afagamesbd");
            $sql = "SELECT * FROM juegos order by idjuego desc";
            $rta = mysqli_query($conn, $sql);
            while ($mostrar = mysqli_fetch_row($rta)) {
            ?>
                <tr>
                    <td><?php echo $mostrar['0'] ?></td>
                    <td><?php echo $mostrar['1'] ?></td>
                    <td><?php echo $mostrar['2'] ?></td>
                    <td><?php echo $mostrar['3'] ?></td>
                    <td><?php echo $mostrar['4'] ?></td>
                    <td>
                        <a class="enlace" href="editar.php?
                        idjuego=<?php echo $mostrar['0'] ?> &
                        nombre=<?php echo $mostrar['1'] ?> &
                        descripcion=<?php echo $mostrar['2'] ?> &
                        categoria=<?php echo $mostrar['3'] ?> &
                        preciojuego=<?php echo $mostrar['4'] ?>
                        ">Editar</a>
                        <a class="enlace" href="sp_eliminar.php">Eliminar</a>
                    </td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
</body>

</html>