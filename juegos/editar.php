<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Juego</title>
</head>

<body>
    <?php
    $idjuego= $_GET['idjuego'];
    $nombre = $_GET['nombre'];
    $descripcion = $_GET['descripcion'];
    $categoria = $_GET['categoria'];
    $preciojuego = $_GET['preciojuego'];
    ?>
    <div>
        <form action="sp_editar.php" method="post">
            <table border="1">
                <tr>
                    <td>Ingresar Datos</td>
                    <td><input type="number" name="idjuego" id="" value="<?=$idjuego?>"></td>
                </tr>
                <tr>
                    <td>Nombre del Juego</td>
                    <td><input type="text" name="nombre"  id="" value="<?=$nombre?>"></td>
                </tr>
                <tr>
                    <td>Descripcion</td>
                    <td><input type="text" name="descripcion" id="" value="<?=$descripcion?>"></td>
                </tr>
                <tr>
                    <td>Categoria</td>
                    <td><input type="number" name="categoria" id="" value="<?=$categoria?>"></td>
                </tr>
                <tr>
                    <td>Precio</td>
                    <td><input type="number" name="preciojuego" id="" value="<?=$preciojuego?>"></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Guardar Cambios"></td>
                    <td><a href="Index.php">Cancelar</a></td>
                </tr>
            </table>
        </form>
    </div>
</body>

</html>