-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-11-2022 a las 00:12:33
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `afagamesbd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carritocompras`
--

CREATE TABLE `carritocompras` (
  `idcarrito` int(10) NOT NULL,
  `idjuego_juegos` int(10) NOT NULL,
  `idcat_categorias` int(15) NOT NULL,
  `cantidadjuegos` int(2) NOT NULL,
  `valortotal` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idcategoria` int(11) NOT NULL,
  `nombrecat` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idcategoria`, `nombrecat`) VALUES
(1, 'Aventura'),
(2, 'Musica'),
(3, 'Deportes'),
(4, 'Terror'),
(5, 'Disparos'),
(6, 'Plataforma'),
(7, 'Simulacion'),
(8, 'RPG'),
(9, 'Accion'),
(10, 'Carreras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegos`
--

CREATE TABLE `juegos` (
  `idjuego` int(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripción` varchar(250) NOT NULL,
  `idcat_categoria` int(15) NOT NULL,
  `preciojuego` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `juegos`
--

INSERT INTO `juegos` (`idjuego`, `nombre`, `descripción`, `idcat_categoria`, `preciojuego`) VALUES
(3, 'Alice Madness Returns', 'Continuación del mero juego re chimba que retrata la vida de Alice Lidell (Alicia en el pais de las maravillas)de una manera en la que no se espera. Juego 10 de 10', 1, 80000),
(4, 'Deemo', 'Juego de musica en la que debes ayudar a Alice (prota) a despertar de su coma haciendo crecer un arbol que te lleva a la salida. Deemo es la parte de la conciencia del hermano mayor que fue el que murió por salvarla :\'(', 2, 72000),
(5, 'Tomb Raider', 'Pos el juego es entretenido y esa Lara es re barbara pal parcore mis perros.', 1, 80000),
(6, 'American Truck Simulator', 'Aprende a manejar camion por el mundo. :)', 7, 42000),
(7, 'Fifa 23', 'Fifita fifita!!!', 3, 170000),
(8, 'The Legend Of Zelda BotW', 'Juego de mundo abierto en donde puedes hacer la historia desde el punto que quieras', 1, 25000),
(9, 'Crash Bandicoot', 'Remake de la trilogia del popular juego de PS1', 1, 53000),
(10, 'Super Mario Bros 3', 'Juego clásico de la NES remasterizado con los gráficos de GBA', 6, 9000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carritocompras`
--
ALTER TABLE `carritocompras`
  ADD PRIMARY KEY (`idcarrito`),
  ADD KEY `idjuego_juegos` (`idjuego_juegos`),
  ADD KEY `nombrecat_categorias` (`idcat_categorias`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `juegos`
--
ALTER TABLE `juegos`
  ADD PRIMARY KEY (`idjuego`),
  ADD KEY `nombrecat_categoria` (`idcat_categoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carritocompras`
--
ALTER TABLE `carritocompras`
  MODIFY `idcarrito` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `juegos`
--
ALTER TABLE `juegos`
  MODIFY `idjuego` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carritocompras`
--
ALTER TABLE `carritocompras`
  ADD CONSTRAINT `carritocompras_ibfk_1` FOREIGN KEY (`idjuego_juegos`) REFERENCES `juegos` (`idjuego`) ON UPDATE CASCADE,
  ADD CONSTRAINT `carritocompras_ibfk_2` FOREIGN KEY (`idcat_categorias`) REFERENCES `categorias` (`idcategoria`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `juegos`
--
ALTER TABLE `juegos`
  ADD CONSTRAINT `juegoscat_ibfk_1` FOREIGN KEY (`idcat_categoria`) REFERENCES `categorias` (`idcategoria`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
